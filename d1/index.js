// npm init
/*
	- triggering this command "npm init" will prompt the user for different settings that will define the application
	- using this command will create a "package.json" in our repo
	- package.json tracks the version of our application, depending on the settings we have set. Also, we can see the dependencies that we have installed inside of the file.
*/



// npm install express
/*
	- after triggering this command, express will now be listed as a "dependency". This can be seen inside the "package.json" file under the "dependencies" property

	- installing any dependencies using npm will result into creating "node_modules" folder and "package-lock.json"
		"node_modules" directory should be left on the local repository because some of t he hosting sites will fail to load the repository once it found the "node_modules" inside the repo. Another reason why node_modules is left on the local repository is becuase it takes too much time to commit.
		"node_modules" is also where the dependencies needed files are stored

	- ".gitignore" files, as the name suggests, will tell the git which files are to be spared/"ignored" in terms of commiting and pushing.

	- "npm install" - this command is used when there are available dependencies inside our "package.json" but are not yet installed inside the repo/project. This is useful when trying to clone a report from a git repository to our local repository.
*/

// TEMPLATE
/*
const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended: true}));

app.listen(port, () => console.log(`Server running at port: ${port}`));
*/


// we need now the express module since in this dependency, it has already built-in codes that will allow the dev to create a server easier.
const express = require("express");

// express() - allows us to create our server using express
// simply put, the app variableis our server
const app = express();

//setting up port variable
const port = 3000;

// app.use lets the server handle data from requests
// 'app.use(express.json());' - allows the app to read and handle json data types.
// methods used from express middlewares	
	// middleware - software that provide common services for the application (ex. pathing and http methods)

app.use(express.json());

// 'app.use(express.urlencoded({extended: true}));' allows the server to read data from forms.
// by default, information received from the URL can only be received as a string or an array.
// but using 'extended:true' for the argument allows us to receive information from other data types such as objects which we will use throughout our application.
app.use(express.urlencoded({extended: true}));


// SECTION - Routes

// GET Method
/*
	Express has methods corresponding to each HTTP methods
		the route below expects to receive a GET request at the base URI "/"
*/
app.get("/", (req,res) => {
	res.send("Hello World");
});

/*
	create a '/hello' URI point which will send a response of "Hello from /hello endpoint"
*/
app.get("/hello", (req,res) => {
	res.send("Hello from /hello endpoint");
})


// same content multiple endpoints: array of endpoints
/*
app.get(["/hello","/endpoint2"] (req,res) => {
	res.send("Hello from /hello endpoint");
})
*/

// POST Method
	// this route expects to receive a POST request at the URI "/hello"
app.post("/hello", (req,res) => {
	// req.body contains the contents/data of the request body
	// the properties defined in Postman request will be accessible here as properties with the same name.
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});


let users = [
	{firstName:"Jane", lastName:"Doe"},
	{firstName:"John", lastName:"Smith"},
	{firstName:"Ji Eun", lastName:"Lee"},
	{firstName:"Kimmy", lastName:"Schmidt"}
];


app.post("/signup", (req,res) => {
	console.log(req.body);
	/*
		create an if-else statement stating that 
			- if firstName and lastName are not null, the object will be pushed into the users array and the server will send a message confirming that the user has signed up
			- else, send a message stating that firstName and lastName should be filled with info
	*/

	if (req.body.firstName !== "" && req.body.lastName !== ""){
		users.push(req.body);
		console.log(users);
		res.send(`User ${req.body.firstName} ${req.body.lastName} successfully added`);
	}

	else {
		res.send("Please add a firstName and lastName.");
	};


app.put("/change-lastName",(req,res) =>{
	// initialized a variable to be used in the selection control structure codes
	let message;

	// loop through the elements of the "users" array
	for(let i=0; i < users.length; i++){
		if(req.body.firstName == users[i].firstName){

			users[i].lastName = req.body.lastName;

			message = `User ${req.body.firstName} has successfully changed the lastName into ${req.body.lastName}`;

			// will terminate the loop if there is a match
			break;
		}
		// if no user is found
		else{
			message = "User does not exist";
		}
	}
	res.send(message);
	console.log(users);
})

})



// tells our server to listen to the port
// if the port is accessed, we can run the server,
// returns a message to confirm that the server is running.
app.listen(port, () => console.log(`Server running at port: ${port}`));











// SECTION - ACTIVITY


// Create a GET route that will access the "/home" route that will print out a simple message.
app.get("/home", (req,res) => {
	res.send("Welcome to the home page");
})


//Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.

app.get("/users", (req,res) => {
	console.log(users);
	res.send(users);
})


// Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.

app.delete("/delete-user", (req,res) => {
	let message;

	for(let i=0; i < users.length; i++){
		if(req.body.firstName == users[i].firstName){
			message = `User ${users[i].firstName} ${users[i].lastName} has been successfully deleted`;
			users.splice(i,1);
			break;
		}
		else{
			message = "User does not exist";
		}
	}
	res.send(message);
	console.log(users);
	
})